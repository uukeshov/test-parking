package com.epam.parking.executor;

import com.epam.parking.common.Utils;
import com.epam.parking.model.Slot;
import com.epam.parking.service.AsyncService;
import com.epam.parking.service.SlotManageService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;

@Slf4j
@EnableScheduling
@EnableAsync
@AllArgsConstructor
@Configuration
public class TrafficIn {

    private final SlotManageService slotManageService;
    private final AsyncService asyncService;

    @PostConstruct
    void init() {
        for (int i = 0; i < 20; i++) {
            slotManageService.createPlace(Slot.builder()
                    .ttl(LocalDateTime.now())
                    .isEmpty(true)
                    .level(1)
                    .build());
        }

        for (int i = 0; i < 20; i++) {
            slotManageService.createPlace(Slot.builder()
                    .ttl(LocalDateTime.now())
                    .isEmpty(true)
                    .level(2)
                    .build());
        }

        log.info("Data preloaded!");

    }

    @Scheduled(initialDelay = 1000, fixedRate = 100)
    public void carsOut() {
        slotManageService.releasePlace();
    }


}
