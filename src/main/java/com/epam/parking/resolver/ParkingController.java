package com.epam.parking.resolver;

import com.epam.parking.service.AsyncService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;

@AllArgsConstructor
@RestController
public class ParkingController {

    private final AsyncService asyncService;

    @PostMapping(value = "entrance1/{carnumber}")
    public ResponseEntity<?> entrance1(@PathVariable String carnumber) {
        var res = asyncService.execute(carnumber);
        try {
            return ResponseEntity.ok().body(res.get());
        } catch (InterruptedException | ExecutionException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PostMapping(value = "entrance2/{carnumber}")
    public ResponseEntity<?> entrance2(@PathVariable String carnumber) throws ExecutionException, InterruptedException {
        var res = asyncService.execute(carnumber);
        try {
            return ResponseEntity.ok().body(res.get());
        } catch (InterruptedException | ExecutionException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PostMapping(value = "entrance3/{carnumber}")
    public ResponseEntity<?> entrance3(@PathVariable String carnumber) throws ExecutionException, InterruptedException {
        var res = asyncService.execute(carnumber);
        try {
            return ResponseEntity.ok().body(res.get());
        } catch (InterruptedException | ExecutionException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PostMapping(value = "entrance4/{carnumber}")
    public ResponseEntity<?> entrance4(@PathVariable String carnumber) throws ExecutionException, InterruptedException {
        var res = asyncService.execute(carnumber);
        try {
            return ResponseEntity.ok().body(res.get());
        } catch (InterruptedException | ExecutionException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

}
