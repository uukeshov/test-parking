package com.epam.parking.repo;

import com.epam.parking.model.Slot;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface SlotRepository extends JpaRepository<Slot, String> {

    List<Slot> getAllByTtlLessThan(LocalDateTime localDateTime);
    Integer countByisEmptyIsTrue();
    Optional<Slot> findTopByisEmptyIsTrue();
}
