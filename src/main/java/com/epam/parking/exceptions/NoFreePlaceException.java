package com.epam.parking.exceptions;

public class NoFreePlaceException extends RuntimeException {
    public NoFreePlaceException(String message) {
        super(message);
    }
}
