package com.epam.parking.service;

import com.epam.parking.dto.SlotDto;
import com.epam.parking.model.Slot;

public interface SlotManageService {
    SlotDto takePlace(String car);
    void releasePlace();
    Slot createPlace(Slot slot);
}
