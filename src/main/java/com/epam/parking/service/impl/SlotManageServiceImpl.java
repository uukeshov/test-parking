package com.epam.parking.service.impl;

import com.epam.parking.dto.SlotDto;
import com.epam.parking.exceptions.NoFreePlaceException;
import com.epam.parking.model.Slot;
import com.epam.parking.repo.SlotRepository;
import com.epam.parking.service.SlotManageService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Slf4j
@AllArgsConstructor
@Service
public class SlotManageServiceImpl implements SlotManageService {

    private final SlotRepository slotRepository;

    @Transactional
    @Override
    public synchronized SlotDto takePlace(String carNumber) {
        var slot = getFreeSlot();
        try {
            slot.setEmpty(false);
            slot.setCarNumber(carNumber);
            slot.setTtl(LocalDateTime.now().plusMinutes(20));
            slotRepository.save(slot);
            log.info("Result : {} ", slot);
        } catch (NoFreePlaceException noFreePlaceException) {
            log.error("SlotManageServiceImpl.takePlace {}", noFreePlaceException.getMessage());
            throw new NoFreePlaceException("No free slots!");
        }

        return SlotDto.builder()
                .slotId(slot.getId())
                .ttl(slot.getTtl())
                .level(slot.getLevel())
                .build();
    }

    @Override
    public void releasePlace() {
        slotRepository.getAllByTtlLessThan(LocalDateTime.now())
                .forEach(slot -> {
                    slot.setCarNumber(null);
                    slot.setEmpty(true);
                    slotRepository.save(slot);
                });
    }

    @Override
    public Slot createPlace(Slot slot) {
        return slotRepository.save(slot);
    }

    private Slot getFreeSlot() {
        return slotRepository.findTopByisEmptyIsTrue()
                .orElseThrow(() -> new NoFreePlaceException("No Free slot!"));
    }
}
