package com.epam.parking.service.impl;

import com.epam.parking.dto.SlotDto;
import com.epam.parking.service.AsyncService;
import com.epam.parking.service.SlotManageService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

@AllArgsConstructor
@Slf4j
@Service
public class AsyncServiceImpl implements AsyncService {

    private final SlotManageService slotManageService;

    @Override
    public Future<SlotDto> execute(String carNumber) {
        log.info("current thread name {}", Thread.currentThread().getName());
        return CompletableFuture.completedFuture(slotManageService.takePlace(carNumber));
    }
}
