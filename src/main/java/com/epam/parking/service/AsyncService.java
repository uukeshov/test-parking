package com.epam.parking.service;

import com.epam.parking.dto.SlotDto;
import org.springframework.scheduling.annotation.Async;

import java.util.concurrent.Future;

public interface AsyncService {
    @Async
    Future<SlotDto> execute(String carNumber);
}
