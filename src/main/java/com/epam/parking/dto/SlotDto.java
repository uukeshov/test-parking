package com.epam.parking.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Builder
@Data
public class SlotDto  {
    private Long slotId;
    private Integer level;
    private LocalDateTime ttl;
}
