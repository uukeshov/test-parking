package com.epam.parking.service.impl;

import com.epam.parking.ParkingApplication;
import com.epam.parking.common.Utils;
import com.epam.parking.exceptions.NoFreePlaceException;
import com.epam.parking.repo.SlotRepository;
import com.epam.parking.service.AsyncService;
import com.epam.parking.service.SlotManageService;
import org.junit.Rule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.LocalDateTime;
import java.util.concurrent.ExecutionException;

@SpringBootTest(classes = ParkingApplication.class)
@Testcontainers
@ActiveProfiles("test-containers")
class SlotManageServiceImplTest {

    @Autowired
    private SlotRepository slotRepository;

    @Autowired
    private AsyncService asyncService;

    @Autowired
    private SlotManageService slotManageService;


    @Test
    void test_order_place() throws ExecutionException, InterruptedException {
        var result = asyncService.execute(Utils.generateCarNumber());
        Assertions.assertNotNull(result.get().getSlotId());
    }

    @Test
    void test_release_place() {
        var slot = slotRepository.findTopByisEmptyIsTrue().orElseThrow(() -> new NoFreePlaceException("No free place!"));
        slot.setTtl(LocalDateTime.now().minusMinutes(100));
        slot.setEmpty(false);
        slotRepository.save(slot);
        var before = slotRepository.countByisEmptyIsTrue();
        slotManageService.releasePlace();
        var after = slotRepository.countByisEmptyIsTrue();

        Assertions.assertNotEquals(before, after);

    }

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    void test_full_place() {
        for (int i = 0; i < 40; i++) {
            asyncService.execute(Utils.generateCarNumber());
        }

        exception.expect(NoFreePlaceException.class);
        var result = asyncService.execute(Utils.generateCarNumber());

    }
}